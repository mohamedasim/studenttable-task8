export class Student {
    public id: number;
    public sname: string;
    public fname: string;
    public gender: string;
    public date: string;
    public dep: string;
    public address: string;
    public email: string;
    public age: number;
    public pass: string;
    public repass: string;
    public checkbox: boolean;
}
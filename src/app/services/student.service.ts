import { Injectable, ɵɵqueryRefresh } from '@angular/core';
import { Student } from '../models/student.model';

@Injectable({
  providedIn: 'root'
})

export class StudentService {

  students: Student[] = [];
  constructor() { }
  onGet() {
    this.students =  JSON.parse(localStorage.getItem('table'));
    return this.students;
  }



  onGetStudent(id: Number) {
    debugger;
    this.students =  JSON.parse(localStorage.getItem('table'));
    let stu:Student = this.students.find(x=>x.id == id);
    let stuF:Student[] = this.students.filter(x=>x.id == id);
    return stuF;
  }
  onAdd(student: Student) {
 //   this.students.push(student);

    let tableData = [];

    let SData = JSON.parse(localStorage.getItem('table'));
    if (SData)
      tableData = SData;

    let finddata =   SData.find(x=>x.id === student.id); 
    if (!finddata) {
      tableData.push(student);
    }
    else {
      alert("id cant to be dublicate");
      return false;
    }
    let table = JSON.stringify(tableData);

    localStorage.setItem('table', table);
   // localStorage.getItem('table');
   return true;
  }

  onDelete(id: Number) {
    
    let tableData = [];

    let SData = JSON.parse(localStorage.getItem('table'));
      let student = SData.find(x=>x.id == id);
    let index = SData.indexOf(student,0);
    SData.splice(index,1);
    confirm("the record will be deleted");
    let table = JSON.stringify(SData);
localStorage.setItem('table', table);

  }

  onUpdate(student: Student) {
    
    let SData:Student[] = JSON.parse(localStorage.getItem('table'));
   
    for (let index = 0; index < SData.length; index++) {
    
      if(SData[index].id==student.id) {
        SData[index] = student;
        break;
      }
      
    }

    let table = JSON.stringify(SData);
    localStorage.setItem('table', table);

  }
}

import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm, Validators, FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { StudentService } from 'src/app/services/student.service';
import { Student } from 'src/app/models/student.model';
import { empty } from 'rxjs';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  form = new FormGroup({
    id: new FormControl('',Validators.required),
    sname: new FormControl('',Validators.required),
    fname: new FormControl('',Validators.required),
    date: new FormControl('',Validators.required),
    dep: new FormControl('',Validators.required),
    address: new FormControl('',Validators.required),
    email: new FormControl('',[Validators.required, Validators.email]),
    age: new FormControl('',Validators.required),
    pass: new FormControl('',[Validators.required, Validators.minLength(6)]),
    repass: new FormControl('',[Validators.required, Validators.minLength(6)]),
    gender: new FormControl(''),
    checkbox: new FormControl('')
  });
  //id: number;
  header: string;
  student: Student = {
    id: 0,
    sname: '',
    fname: '',
    gender: '',
    date: '',
    dep: '',
    address: '',
    email: '',
    age: 0,
    pass: '',
    repass: '',
    checkbox: false,
  };
  constructor(private router: Router, private route: ActivatedRoute, private studentService: StudentService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    debugger;
    if(this.viewType !="Popup")
     this.id = this.route.snapshot.paramMap.get('id');

    this.header = this.id === 0 ? 'Add Student' : 'Edit Student';
    this.getformValue();
  }

  @Input() showpopup:boolean =true;
  @Input() id:any =0;
  @Input() viewType:any = null;
  @Output() closeEdit = new EventEmitter();



  getformValue() {

    debugger;
    if (this.id != 0) {
      let student:Student = this.studentService.onGetStudent(this.id)[0];
     debugger;

     this.form.setValue({
      id:student.id,
    sname:student.sname,
    fname:student.fname,
    date: student.date,
    gender:student.gender,
    dep: student.dep,
    address:student.address,
    email:student.email,
    age: student.age,
    pass:student.pass,
    repass:student.repass,
    checkbox:student.checkbox
    })

    }

  }

  onSubmit() {

    debugger;

    let studentValue = this.form.value;

    if (this.id === 0) {
      let rdata = this.studentService.onAdd(studentValue);
      if (rdata) {
        alert("Created sucessfully");
        if (this.viewType != "Popup")
          this.closeEdit.emit();
        else
          this.router.navigate(['/']);
      }
    }
    else {
      this.studentService.onUpdate(studentValue);
      alert("updated sucessfully");
      if (this.viewType != "Popup")
        this.closeEdit.emit();
      else
        this.router.navigate(['/']);
    }
  }
  resetform() {
    this.student = {
      id: 0,
      sname: '',
      fname: '',
      gender: '',
      date: '',
      dep: '',
      address: '',
      email: '',
      age: 0,
      pass: '',
      repass: '',
      checkbox: false,
    };
  }

  closeEditpopup() {
    debugger;
    this.closeEdit.emit();
  }
}


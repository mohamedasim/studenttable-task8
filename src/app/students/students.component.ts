import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/services/student.service';
import { Student } from 'src/app/models/student.model';
import { Form } from '@angular/forms';
import { formatCurrency } from '@angular/common';
// import { MatSort } from '@angular/material';
@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  D: Student[];
  data: Array<any>;
  totalRecords: number;
  page: number = 1;
  constructor(private studentService: StudentService)
  {this.data = new Array<any>()}

  ngOnInit(): void {
    this.getTableData();
  }

  onDelete(id: number) {

    this.studentService.onDelete(id);
    this.getTableData();
    
  }
  getTableData() {
    this.D = this.studentService.onGet();
  }
  showEditForm:boolean = false;

  EditValue:number = 0;

  openEdit(id: number) {
    debugger;
    this.showEditForm = true;
    this.EditValue = id;
  }
  closeEditfromStudentPage() {
    debugger;
    this.showEditForm = false;
    this.getTableData();
  }

  openCreate() {
    debugger;
    this.showEditForm = true;
    this.EditValue = 0;
  }
}
